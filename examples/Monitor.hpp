#include "systemc.h"
class Monitor: public sc_module {
public:
	sc_fifo_in<double> in;
	double data;

private:
	void consume(void) {
		while(true) {
			data = in.read();
			cout << "monitor: " << data << std::endl;
		}
	}

public:
	SC_HAS_PROCESS(Monitor);

	Monitor(sc_module_name name) : sc_module(name) {
		SC_THREAD(consume);
	}
};
